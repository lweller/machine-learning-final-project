import codecs
import json
import re
import pprint
import subprocess
import os
tweetsPath = "C:/Users/NKNSc/Documents/test"
fileNames = ["tweetsCats.json", "tweetsDarkPhoenix.json", "tweetsendgame.json", "tweetsJoker.json", "tweetsOUATIH.json",
             "tweetsTerminator.json",
             "tweetsTLK.json", "tweetsUs.json"]
outNames = ["cats_out.txt", "DarkPhoenix_out.txt", "endgame_out.txt", "joker_out.txt", "OUATIH_out.txt",
            "Terminator_out.txt","TLK_out.txt"]
javaPath = "C:\\Program Files\\Java\\jdk-14.0.1\\bin"

def parse_json(file):
    with codecs.open(file) as f:
        tweets = json.load(f, encoding='utf-8')
    return list(map(lambda x: x["text"].replace("\n", "").replace('"',"").replace("'", "").replace(".","").replace("?","").replace("!","") + ".", tweets))


def save_to_file(tweets, name):
    joiner = "\n"
    output = joiner.join(tweets[:700])
    file = codecs.open(name,"w", "utf-8")
    file.write(output)
    file.close()

def save_all():
    for file in fileNames:
        f = parse_json(tweetsPath + "/" + file)
        save_to_file(f, file[:-4]+"txt")

def normalize_all():
    for file in outNames:
        normalize(tweetsPath + "/" + file)

def normalize(fileName):
    file = codecs.open(fileName, "r", "cp1252")
    lines = file.readlines()
    output = []

    for line in lines:
        line = line.strip()
        print(line)
        if line == "Very Negative":
            output.append("-2")
        elif line == "Negative":
            output.append("-1")
        elif line == "Neutral":
            output.append("0")
        elif line == "Positive":
            output.append("1")
        elif line == "Very Positive":
            output.append("2")
    joiner = ","
    output = joiner.join(output)
    print(output)
    file = open(fileName[:-4]+"_results.csv","w")
    file.write(output)
    file.close()


save_all()
normalize_all()

