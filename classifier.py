#!/usr/bin/env python3
import pandas as pd
import statistics as stat
import warnings

from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC

from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score

from sklearn.exceptions import UndefinedMetricWarning
from sklearn.exceptions import ConvergenceWarning

# from IPython.display import Image
# from sklearn.tree import export_graphviz
# import pydotplus # 2.0.2
# import matplotlib.pyplot as plt

def build_success_labels(df):
	financial_success_column = []
	critical_success_column  = []
	audience_success_column  = []

	for index, row in df.iterrows():
		financial_success_column.append(financial_success(row['budget'], row['worldwide_gross']))
		critical_success_column.append(critical_success(row['rt_critic_score'], row['mc_critic_score']))
		audience_success_column.append(audience_success(row['rt_audience_score'], row['mc_audience_score']))

	df['financial_success'] = financial_success_column
	df['critical_success']  = critical_success_column
	df['audience_success']  = audience_success_column

	return df

def financial_success(budget, worldwide_gross):
	profit_factor = worldwide_gross/budget

	# Threshold of pf > 6.22 provides exactly a 1:1 ratio between
	# movies classified as hits and movies classified as flops
	if profit_factor > 6.22:
		return 1 # HIT
	else:
		return 0 # FLOP

def critical_success(rt_critic_score, mc_critic_score):
	combined_score = (int(rt_critic_score[:-1]) + int(mc_critic_score)) / 200

	# Threshold of cs > 0.825 provides exactly a 1:1 ratio between
	# movies classified as hits and movies classified as flops
	if combined_score > 0.825:
		return 1 # HIT
	else:
		return 0 # FLOP

def audience_success(rt_audience_score, mc_audience_score):
	combined_score = (int(rt_audience_score[:-1]) + 10 * int(mc_audience_score)) / 200

	# Threshold of cs > 0.71 provides exactly a 1:1 ratio between
	# movies classified as hits and movies classified as flops
	if combined_score > 0.71:
		return 1 # HIT
	else:
		return 0 # FLOP

if __name__ == '__main__':
	# Loads df
	df = pd.read_csv('movies_db.csv')

	# Labels the financial, critical, and audience success of movies in df
	df = build_success_labels(df)

	# Sets train:test ratio of 80:20
	test_size = int(len(df)*0.2)
	
	# Sets optimal k value for cross-validation range
	k = int(len(df)/test_size)

	# Turn off UndefinedMetricWarning warnings from sklearn
	warnings.filterwarnings(action='ignore', category=UndefinedMetricWarning)
	warnings.filterwarnings(action='ignore', category=ConvergenceWarning)

	for label_type in ['financial_success', 'critical_success', 'audience_success']:
		# Partitions into features and labels
		x = df[['budget','mean_tweet_score','tweet_score_stddev']]
		y = df[label_type]

		# Lists to record metrics of interest
		accuracies = []
		recalls = []
		precisions = []
		f1_scores = []

		if label_type == 'financial_success':
			print('Financial Success:\n------------------')
		elif label_type == 'critical_success':
			print('Critical Success:\n-----------------')
		else: 
			print('Audience Success:\n-----------------')

		# Repeats * times to more accurately determine evaluation metrics
		for _ in range(100):
			
			# Randomly shuffles df
			df = df.sample(frac=1).reset_index(drop=True)

			for cv in range(k):
			    
			    # Partitions train set and test set based on current cv
			    x_train = x.iloc[:cv*test_size].append(x.iloc[(cv+1)*test_size:])
			    y_train = y[:cv*test_size].append(y[(cv+1)*test_size:])
			    x_test = x.iloc[cv*test_size:(cv+1)*test_size]
			    y_test = y[cv*test_size:(cv+1)*test_size]

			    # Creates a model and fits it on training data
			    model = LinearSVC()
			    model.fit(x_train, y_train)

			    # Gets evaluation metrics
			    accuracies.append(accuracy_score(y_test, model.predict(x_test)))
			    recalls.append(recall_score(y_test, model.predict(x_test), average="macro"))
			    precisions.append(precision_score(y_test, model.predict(x_test), average="macro"))
			    f1_scores.append(f1_score(y_test, model.predict(x_test), average="macro"))

		print("Accuracy: {:0.4f}, StdDev: {:0.4f}".format(sum(accuracies) / len(accuracies), stat.stdev(accuracies)))
		print("Recall: {:0.4f}, StdDev: {:0.4f}".format(sum(recalls) / len(recalls), stat.stdev(recalls)))
		print("Precision: {:0.4f}, StdDev: {:0.4f}".format(sum(precisions) / len(precisions), stat.stdev(precisions)))
		print("F1 Score: {:0.4f}, StdDev: {:0.4f}".format(sum(f1_scores) / len(f1_scores), stat.stdev(f1_scores)))

		print()



