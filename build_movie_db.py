#!/usr/bin/env python3
import requests, sys, statistics, time
from bs4 import BeautifulSoup

def initialize_db(db_filename):
	with open(db_filename, 'w') as db:
		db.write('title,year,budget,worldwide_gross,rt_critic_score,rt_audience_score,mc_critic_score,mc_audience_score,total_tweets,mean_tweet_score,total_tweet_score,tweet_score_stddev\n')

def add_db_entry_db(title, year, db_filename):
	rt_scores, metacritic_scores, sentiment_analysis, box_office_numbers = master_lookup(title, year)

	db_entry = '{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(title,year,box_office_numbers[0],box_office_numbers[1],rt_scores[0],rt_scores[1],metacritic_scores[0],metacritic_scores[1],sentiment_analysis[0],sentiment_analysis[1],sentiment_analysis[2],sentiment_analysis[3])

	print('Adding entry to db: {}'.format(db_entry), end='')

	with open(db_filename, 'a') as db:
		db.write(db_entry)

def master_lookup(title, year):
	rt_scores = rt_lookup(title, year)
	metacritic_scores = metacritic_lookup(title, year)
	sentiment_analysis = sentiment_analysis_lookup(title)
	box_office_numbers = box_office_lookup(title)

	return rt_scores, metacritic_scores, sentiment_analysis, box_office_numbers

def rt_lookup(title, year):
	base_url = 'https://www.rottentomatoes.com/m/'

	title = title.lower().replace(':', '').replace(' ', '_')
	alt_title = title + '_' + str(year)

	response = requests.get(base_url + alt_title)

	if response.status_code == 404:
		response = requests.get(base_url + title)

	if response.status_code != 200:
		sys.exit('RT: Could not scrape movie with title: {} or alternate title: {}, Status Code: {}'.format(title, alt_title,response.status_code))

	soup = BeautifulSoup(response.content, 'html.parser')

	ratings = [rating.text.strip() for rating in soup.find_all('span', class_='mop-ratings-wrap__percentage')]

	return ratings[0], ratings[1]

def metacritic_lookup(title, year):
	# Headers required for scraping Metacritic (403 Error)
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36'}

	base_url = 'https://www.metacritic.com/movie/'

	title = title.lower().replace(':', '').replace(' ', '-')
	alt_title = title + '-' + str(year)

	# Metacritic's servers are easily spooked into throwing 429 errors for too many requests
	# If requests keep failing, uncomment the following lines to sleep between each request
	# print("Sleeping for 60s ...")
	# time.sleep(60)
	
	response = requests.get(base_url + alt_title, headers=headers)

	if response.status_code == 404:
		response = requests.get(base_url + title, headers=headers)

	if response.status_code != 200:
		sys.exit('Metacritic: Could not scrape movie with title: {} or alternate title: {}, Status Code: {}'.format(title, alt_title,response.status_code))

	soup = BeautifulSoup(response.content, 'html.parser')

	critical_rating = soup.find('span', class_='metascore_w larger movie positive')
	if critical_rating is None:
		critical_rating = soup.find('span', class_='metascore_w larger movie mixed')
	if critical_rating is None:
		critical_rating = soup.find('span', class_='metascore_w larger movie negative')

	audience_rating = soup.find('span', class_='metascore_w user larger movie positive')
	if audience_rating is None:
		audience_rating = soup.find('span', class_='metascore_w user larger movie mixed')
	if audience_rating is None:
		audience_rating = soup.find('span', class_='metascore_w user larger movie negative')

	return critical_rating.text, audience_rating.text

def sentiment_analysis_lookup(title):
	filename = 'tweet-results/{}_results.csv'.format(title.lower().replace(': ','_').replace(' ','_'))

	with open(filename, 'r') as results:
		raw_scores = results.read()

	sentiment_scores = [int(score) for score in raw_scores.split(',')]

	total_tweets = len(sentiment_scores)
	mean_score   = statistics.mean(sentiment_scores)
	total_score  = sum(sentiment_scores)
	stddev_score = statistics.stdev(sentiment_scores)
	
	return total_tweets, mean_score, total_score, stddev_score

def box_office_lookup(title):
	# budget and box-office numbers are hard-coded here
	# we're currently unable to find a scrapable website containing the desired financial information
	if 	 title == 'Avengers: Endgame': return '365000000', '2797800564'
	elif title == 'The Lion King': return '260000000', '1656943394'
	elif title == 'Joker': return '55000000', '1074148104'
	elif title == 'Us': return '20000000','255184580'
	elif title == 'Once Upon a Time in Hollywood': return '90000000','374341301'
	elif title == 'Cats': return '95000000','73874033'
	elif title == 'Dark Phoenix': return '200000000','252442974'
	elif title == 'Terminator: Dark Fate': return '185000000', '261119292'
	elif title == '1917': return '95000000', '368027644'
	elif title == 'Ad Astra': return '90000000', '132807427'
	elif title == 'A Star is Born': return '36000000', '436188866'
	elif title == 'Apollo 11': return '9000000', '15342353'
	elif title == 'Aquaman': return '160000000', '1148461807'
	elif title == 'BlacKkKlansman': return '15000000', '93400823'
	elif title == 'Black Panther': return '200000000', '1346913161'
	elif title == 'Bohemian Rhapsody': return '52000000', '903655259'
	elif title == 'The Beach Bum': return '5000000', '4554416'
	elif title == 'Booksmart': return '6000000', '24863452'
	elif title == 'Captain Marvel': return '160000000', '1128274794'
	elif title == 'Crazy Rich Asians': return '30000000', '238532921'
	elif title == 'Deadpool 2': return '110000000', '785794179'
	elif title == 'Frozen 2': return '150000000', '1450026933'
	elif title == 'Hereditary': return '10000000', '80239658'
	elif title == 'A Hidden Life': return '8000000', '4612788'
	elif title == 'Hustlers': return '20000000', '157563598'
	elif title == 'Incredibles 2': return '200000000', '1242805359'
	elif title == 'Knives Out': return '40000000', '309232797'
	elif title == 'Leave No Trace': return '11000000', '7682928'
	elif title == 'Little Women': return '40000000', '206006503'
	elif title == 'Midsommar': return '9000000', '46890297'
	elif title == 'Pet Sematary': return '21000000', '113118226'
	elif title == 'Parasite': return '11400000', '254218961'
	elif title == 'Spies in Disguise': return '100000000', '171616764'
	elif title == 'Uncut Gems': return '19000000', '50023780'

if __name__ == '__main__':

	db_filename = 'movies_db.csv'

	initialize_db(db_filename)

	with open('movie_list.csv', 'r') as movies:
		for movie in movies.read().split(','):
			add_db_entry_db(movie, 2019, db_filename)
